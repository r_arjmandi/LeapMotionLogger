#ifndef LEAPMOTIONDATAMODEL_H
#define LEAPMOTIONDATAMODEL_H

#include "Leap.h"
#include <QAbstractListModel>

class LeapMotionDataModel: public QAbstractListModel
{
     Q_OBJECT

public:
    LeapMotionDataModel();

    enum LeapMotionDataRoleNames{
        X = Qt::UserRole,
        Y = Qt::UserRole + 2,
        Z = Qt::UserRole + 4
    };

    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    void SetPoints(const std::vector<Leap::Vector>& points);

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    QHash<int, QByteArray> m_LeapMotionDataRoleNames;
    std::vector<Leap::Vector> m_Points;
};

#endif // LEAPMOTIONDATAMODEL_H
