#include "LeapListener.h"
#include <iomanip>
#include "LeapMotionDataModel.h"

//frame
//elbow
//wrist
//THUMB_METACARPAL_BEGIN     THUMB_METACARPAL_CENTER     THUMB_METACARPAL_END
//THUMB_PROXIMAL_BEGIN       THUMB_PROXIMAL_CENTER       THUMB_PROXIMAL_END
//THUMB_INTERMEDIATE_BEGIN   THUMB_INTERMEDIATE_CENTER   THUMB_INTERMEDIATE_END
//THUMB_DISTAL_BEGIN         THUMB_DISTAL_CENTER         THUMB_DISTAL_END

//INDEX_METACARPAL_BEGIN     INDEX_METACARPAL_CENTER     INDEX_METACARPAL_END
//INDEX_PROXIMAL_BEGIN       INDEX_PROXIMAL_CENTER       INDEX_PROXIMAL_END
//INDEX_INTERMEDIATE_BEGIN   INDEX_INTERMEDIATE_CENTER   INDEX_INTERMEDIATE_END
//INDEX_DISTAL_BEGIN         INDEX_DISTAL_CENTER         INDEX_DISTAL_END

//MIDDLE_METACARPAL_BEGIN    MIDDLE_METACARPAL_CENTER    MIDDLE_METACARPAL_END
//MIDDLE_PROXIMAL_BEGIN      MIDDLE_PROXIMAL_CENTER      MIDDLE_PROXIMAL_END
//MIDDLE_INTERMEDIATE_BEGIN  MIDDLE_INTERMEDIATE_CENTER  MIDDLE_INTERMEDIATE_END
//MIDDLE_DISTAL_BEGIN        MIDDLE_DISTAL_CENTER        MIDDLE_DISTAL_END

//RING_METACARPAL_BEGIN      RING_METACARPAL_CENTER      RING_METACARPAL_END
//RING_PROXIMAL_BEGIN        RING_PROXIMAL_CENTER        RING_PROXIMAL_END
//RING_INTERMEDIATE_BEGIN    RING_INTERMEDIATE_CENTER    RING_INTERMEDIATE_END
//RING_DISTAL_BEGIN          RING_DISTAL_CENTER          RING_DISTAL_END

//PINKY_METACARPAL_BEGIN     PINKY_METACARPAL_CENTER     PINKY_METACARPAL_END
//PINKY_PROXIMAL_BEGIN       PINKY_PROXIMAL_CENTER       PINKY_PROXIMAL_END
//PINKY_INTERMEDIATE_BEGIN   PINKY_INTERMEDIATE_CENTER   PINKY_INTERMEDIATE_END
//PINKY_DISTAL_BEGIN         PINKY_DISTAL_CENTER         PINKY_DISTAL_END

LeapListener::LeapListener(std::shared_ptr<LeapMotionDataModel> leapDataModel)
{
    m_LeapDataModel = leapDataModel;
}

void LeapListener::onFrame(const Leap::Controller& controller)
{
    auto frame = controller.frame();
    auto hand = frame.hands()[0];
    auto tabSpace = "\t";
    m_Logger << frameCounter++ << tabSpace;

    std::vector<Leap::Vector> data;

    auto arm = hand.arm();
    auto elbow = arm.elbowPosition();
    m_Logger << std::fixed << std::setprecision(1) << elbow.x << "," << elbow.y << "," << elbow.z << tabSpace;
    data.push_back(elbow);

    auto wrist = arm.wristPosition();
    m_Logger << std::fixed << std::setprecision(1) << wrist.x << "," << wrist.y << "," << wrist.z << tabSpace;
    data.push_back(wrist);

    for(const auto& finger : hand.fingers())
    {
        for(auto i = 0; i < 4; i++)
        {
            Leap::Bone bone;
            switch (i)
            {
            case Leap::Bone::TYPE_METACARPAL:
                bone = finger.bone(Leap::Bone::TYPE_METACARPAL);
                break;

            case Leap::Bone::TYPE_PROXIMAL:
                bone = finger.bone(Leap::Bone::TYPE_PROXIMAL);
                break;

            case Leap::Bone::TYPE_INTERMEDIATE:
                bone = finger.bone(Leap::Bone::TYPE_INTERMEDIATE);
                break;

            case Leap::Bone::TYPE_DISTAL:
                bone = finger.bone(Leap::Bone::TYPE_DISTAL);
                break;
            }

            const auto prevJoint = bone.prevJoint();
            m_Logger << std::fixed << std::setprecision(1) <<  prevJoint.x << "," << prevJoint.y << "," << prevJoint.z << tabSpace;
            data.push_back(prevJoint);

            const auto center = bone.center();
            m_Logger << std::fixed << std::setprecision(1) << center.x << "," << center.y << "," << center.z << tabSpace;
            data.push_back(center);

            const auto nextJoint = bone.nextJoint();
            m_Logger << std::fixed << std::setprecision(1)<< nextJoint.x << "," << nextJoint.y << "," << nextJoint.z << tabSpace;
            data.push_back(nextJoint);

        }
    }
    m_Logger << std::endl;
    m_LeapDataModel->SetPoints(data);
}
