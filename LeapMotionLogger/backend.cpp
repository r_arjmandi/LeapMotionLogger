#include "backend.h"

BackEnd::BackEnd(std::shared_ptr<LeapListener> leapListener):
    m_LeapListener(leapListener)
{
}

void BackEnd::Start()
{
    m_Controller.addListener(*m_LeapListener);
}

void BackEnd::Stop()
{
    m_Controller.removeListener(*m_LeapListener);
}
