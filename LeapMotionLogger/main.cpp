#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "backend.h"
#include "LeapMotionDataModel.h"

static QObject* BackEndProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
static QObject* LeapMotionDataProvider(QQmlEngine *engine, QJSEngine *scriptEngine);

BackEnd *backEnd;
LeapMotionDataModel *leapMotionDataModel;

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);
    leapMotionDataModel = new LeapMotionDataModel;
    auto leapListener = std::make_shared<LeapListener>(std::shared_ptr<LeapMotionDataModel>(leapMotionDataModel));
    backEnd = new BackEnd(std::shared_ptr<LeapListener>(leapListener));

    qmlRegisterSingletonType<BackEnd>("Back.End", 1, 0, "BackEnd", BackEndProvider);
    qmlRegisterSingletonType<LeapMotionDataModel>("LeapMotion.DataModel", 1, 0, "LeapMotionDataModel", LeapMotionDataProvider);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}

static QObject* BackEndProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return backEnd;
}

static QObject* LeapMotionDataProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return leapMotionDataModel;
}
