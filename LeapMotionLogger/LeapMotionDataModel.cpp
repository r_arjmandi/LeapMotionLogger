#include "LeapMotionDataModel.h"

LeapMotionDataModel::LeapMotionDataModel()
{
    m_LeapMotionDataRoleNames[X] = "X";
    m_LeapMotionDataRoleNames[Y] = "Y";
    m_LeapMotionDataRoleNames[Z] = "Z";
}

int LeapMotionDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_Points.size();
}

QVariant LeapMotionDataModel::data(const QModelIndex &index, int role) const
{
    if(m_Points.empty())
    {
        return QVariant();
    }

    int row = index.row();
    if(row < 0 || row >= m_Points.size())
    {
        return QVariant();
    }

    auto data = m_Points.at(row);

    switch(role) {
    case X:
        return data.x;
    case Y:
        return data.y;
    case Z:
        return data.z * -1;
    }
    return QVariant();
}

void LeapMotionDataModel::SetPoints(const std::vector<Leap::Vector> &points)
{
    m_Points = points;
    emit endResetModel();
}

QHash<int, QByteArray> LeapMotionDataModel::roleNames() const
{
    return m_LeapMotionDataRoleNames;
}
