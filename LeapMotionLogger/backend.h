#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <memory>

#include "Leap.h"
#include "LeapListener.h"

class BackEnd : public QObject
{
    Q_OBJECT
public:
    BackEnd(std::shared_ptr<LeapListener> leapListener);
    Q_INVOKABLE void Start();
    Q_INVOKABLE void Stop();

private:
    Leap::Controller m_Controller;
    std::shared_ptr<LeapListener> m_LeapListener;

};

#endif // BACKEND_H
