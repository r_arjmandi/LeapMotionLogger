#ifndef LEAPLISTENER_H
#define LEAPLISTENER_H

#include <fstream>
#include <memory>
#include "Leap.h"

class LeapMotionDataModel;

class LeapListener : public Leap::Listener
{
public:
    LeapListener(std::shared_ptr<LeapMotionDataModel> leapDataModel);
    void onFrame(const Leap::Controller& controller) override;

private:
    std::ofstream m_Logger = std::ofstream("log.txt", std::ios::app);
    long long int frameCounter = 0;
    std::shared_ptr<LeapMotionDataModel> m_LeapDataModel;
};

#endif // LEAPLISTENER_H
